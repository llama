#light

open System
open System.Windows.Forms

let form = new Form()
do form.Width  <- 400
do form.Height <- 300
do form.Text   <- "Llama"

(* text box *)
let textB = new RichTextBox()
do textB.Dock <- DockStyle.Fill
do textB.Text <- "This is a sample F# application to demonstrate command line development to deployment"
do form.Controls.Add(textB)

do Application.Run(form)
