# See tutorial on nmake -
#   http://courses.cs.vt.edu/~cs1704/fall03/Notes/A10.NMAKE.pdf

# You may want to modify this variable to reflect your Wix path
WIXDIR = "C:\Program Files\Windows Installer XML v3\bin"

llama.exe : main.fs
	fsc main.fs -o llama.exe

llama-install.exe: installer.wixobj
    $(WIXDIR)\light.exe installer.wixobj

installer.wixobj: installer.wxs llama.exe
	$(WIXDIR)\candle.exe installer.wxs


# Targets you can use when invoking nmake
llama: llama.exe
installer: llama-install.exe
clean: 
	del *.exe *obj *.msi
